@echo off

echo ==== Running maven with errorprone enabled
rem All this cleaning is expensive (requires recompilation)
rem But "Error-prone is implemented as a compiler hook, using an internal mechanism in javac."
rem https://errorprone.info/docs/installation#my-build-system-isnt-listed-here
rem There might also be other differences (skipped annotation processors, flags, etc.)
rem So I'm not sure that it is OK to keep around the resulting .class file,
rem then mix and match with ones resulting from a regular compilation step.
call mvn clean test -DskipTests -DskipITs -P errorprone > deployment\errorprone\errorprone.log
call mvn clean > nul

echo ==== Generating an html report
perl deployment\errorprone\errorprone.pl < deployment\errorprone\errorprone.log > deployment\errorprone\errorprone.html
del deployment\errorprone\errorprone.log

echo ==== The errorprone report is deployment\errorprone\errorprone.html
