# Changes from M16 to M17

## Applications

* Rainbow

    * Resolved [issue #236](https://gitlab.com/okapiframework/okapi/-/issues/236): Added custom parameters folder default in
      User Preferences.

* Longhorn

    * fixed concurrency issues.
    * fixed problem of reading existing projects.

* Tikal

    * Fixed [issue #249](https://gitlab.com/okapiframework/okapi/-/issues/249) where the HTML filter was not always loaded for
      sub-filter cases (Patch from Mihai)

## Filters

* XLIFF Filter

    * Fixed [issue #218](https://gitlab.com/okapiframework/okapi/-/issues/218) where file name was missing from XML parsing
      error (Patch from Mihai).
    * Fixed [issue #219](https://gitlab.com/okapiframework/okapi/-/issues/219) where empty `<target>` where not created
      instead of being created empty (Patch from Mihai)
    * Fixed the reading of the `coord` target property.

* Plain Text Filter

    * Fixed the link for the help button.
    * Fixed [issue #248](https://gitlab.com/okapiframework/okapi/-/issues/248): Code finder not invoked.

* Table Filter

    * Fixed the link for the help button.
    * Fixed bug when reseting the reader that caused "Marker
      invalid" error in some cases.

* JSON Filter

    * Fixed [issue #214](https://gitlab.com/okapiframework/okapi/-/issues/214) where the rewriting created duplicated
      characters in some cases.

* VersifiedText Filter

    * Added support for Trados-type segment markers.

* XML Filter

    * Fixed [issue #240](https://gitlab.com/okapiframework/okapi/-/issues/240) where code finder matches were not escaped
      properly in some cases.
    * Implemented local withinText in the ITS engine for ITS 2.0
    * Implemented initial support for XPath variables for ITS 2.0
    * Implemented idValue data category for ITS 2.0

    * Removed the Drupal filter. This filter will be addressed in a
      different way later.

## Steps

* Batch Translation Step

    * Added support to allow TMX output to be also the input file.
    * Added option to send the generated TMX as the only input for
      the next step.
    * Added support of the `${inputRootDir}` variable
      for all paths and the command line.
    * Fixed [issue #230](https://gitlab.com/okapiframework/okapi/-/issues/230) where the length of the segment was limited
      to 65000 characters.

* Rainbow Translation Kit Creation Step

    * Added the option to send the prepared files to the next step
      (instead of the filter events).
    * Implemented capability to add support material to the package.
    * Fixed issue with manifest file when zipping the package.
    * Fixed concurrency problems.

* Rainbow Translation Kit Merging Step

    * Added auto-stripping of white spaces before initial text when
      convertingg XLIFF+RTF to XLIFF.

## Libraries

* Verification Library

    * Added `motherTongue` parameter to call for bilingual mode, for
      LanguageTool ([Issue #229](https://gitlab.com/okapiframework/okapi/-/issues/229)).
    * Added option "Verify for same language family" ([Issue #220](https://gitlab.com/okapiframework/okapi/-/issues/220),
      Patch from Mihai).
    * Moved the XLIFF 2.0 library (`okapi-lib-xliff` package) to a
     separate project (http://code.google.com/p/okapi-xliff-toolkit/).
    * Moved the Olifant and TMdb modules to a separate project
     (http://code.google.com/p/okapi-olifant/)
    * Dependent modules are now using the maven2 repository to get the jar.
    * Fixed missing language-only values for LCID mapping.
    * Added support for wildcard characters in `DefaultFilenameFilter`.
      Old constructor with extension only should be backward compatible.
    * Added `getWikiPage()` method to the `AbstractParametersEditor`.
    * Added batch input count support for the `Pipeline Parameters` event.
    * Fixed [issue #233](https://gitlab.com/okapiframework/okapi/-/issues/233). (Patch from Shane Perry): Changed default
      `placeHolderMode` to `true` in `XLIFFWriter`. This affects the direct
      users of the class, indirect users (like kit creations have this
      setting defined by the parameters). Tikal has also been updated to
      force its own mode rather than rely on the default.
    * Fixed [issue #237](https://gitlab.com/okapiframework/okapi/-/issues/237): Removed the old and now defunc special case for
      TTX in the `InputdocumentPanel` dialog when guessing the file format.
      The TTX format should now be guessed properly.
    * Fixed [issue #243](https://gitlab.com/okapiframework/okapi/-/issues/243): ID duplication in sub-filter cases.

