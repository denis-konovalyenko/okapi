/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SlovakSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("sk");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {

		test("This is a sentence. ");

		// NOTE: sentences here need to end with a space character so they
		// have correct whitespace when appended:
		test("Dies ist ein Satz.");
		test("Dies ist ein Satz.", " Noch einer.");
		test("Ein Satz!", " Noch einer.");
		test("Ein Satz...", "Noch einer.");
		test("Unter http://www.test.de gibt es eine Website.");

		test("Das ist,, also ob es bla.");
		test("Das ist es..", " So geht es weiter.");

		test("Das hier ist ein(!) Satz.");
		test("Das hier ist ein(!!) Satz.");
		test("Das hier ist ein(?) Satz.");
		test("Das hier ist ein(???) Satz.");
		test("Das hier ist ein(???) Satz.");

		// ganzer Satz kommt oder nicht:
		test("Das war es: gar nichts.");
		test("Das war es:", " Dies ist ein neuer Satz.");

		// incomplete sentences, need to work for on-thy-fly checking of texts:
		test("Here's a");
		test("Here's a sentence.", " And here's one that's not comp");

		test("„Prezydent jest niemądry”.", " Tak wyszło.");
		test("„Prezydent jest niemądry”, powiedział premier");

		test("Das Schreiben ist auf den 3.10. datiert.");
		test("Das Schreiben ist auf den 31.1. datiert.");
		test("Das Schreiben ist auf den 3.10.2000 datiert.");
		test("Toto 2. vydanie bolo rozobrané za 1,5 roka.");
		test("Festival Bažant Pohoda slávi svoje 10. výročie.");
		test("Dlho odkladané parlamentné voľby v Angole sa uskutočnia 5. septembra.");
		test("Das in Punkt 3.9.1 genannte Verhalten.");

		// From the abbreviation list:
		test("Aké sú skutočné príčiny tzv. transformačných príznakov?");
		test("Aké príplatky zamestnancovi (napr. za nadčas) stanovuje Zákonník práce?");
		test("Počas neprítomnosti zastupuje MUDr. Marianna Krupšová.");
		test("Staroveký Egypt vznikol okolo r. 3150 p.n.l. (tzn. 3150 pred Kr.).", " A zanikol v r. 31 pr. Kr.");

		// from user bug reports:
		test("Temperatura wody w systemie wynosi 30°C.", " W skład obiegu otwartego wchodzi zbiornik i armatura.");
		test("Zabudowano kolumny o długości 45 m.", " Woda z ujęcia jest dostarczana do zakładu.");

		// two-letter initials:
		test("Najlepszym polskim reżyserem był St. Różewicz.", " Chodzi o brata wielkiego poety.");
		test("Nore M. hrozí za podvod 10 až 15 rokov.");
		test("To jest zmienna A.", " Zaś to jest zmienna B.");
		// Numbers with dots.
		test("Mam w magazynie dwie skrzynie LMD20.", " Jestem żołnierzem i wiem, jak można ich użyć");
		// ellipsis
		test("Rytmem tej wiecznie przemijającej światowej egzystencji … rytmem mesjańskiej natury jest szczęście.");
	}

	private void test(final String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}
