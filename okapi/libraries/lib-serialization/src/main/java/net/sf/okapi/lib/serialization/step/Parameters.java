package net.sf.okapi.lib.serialization.step;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

public class Parameters extends StringParameters implements IEditorDescriptionProvider {
    private static final String PRESERVESPACEBYDEFAULT = "preserveWhiteSpaceByDefault";

    public Parameters() {
        super();
    }

    @Override
    public void reset() {
        super.reset();
        setPreserveWhiteSpaceByDefault(false);
    }

    public boolean isPreserveWhiteSpaceByDefault() {
        return getBoolean(PRESERVESPACEBYDEFAULT);
    }

    public void setPreserveWhiteSpaceByDefault(boolean preserveWhiteSpaceByDefault) {
        setBoolean(PRESERVESPACEBYDEFAULT, preserveWhiteSpaceByDefault);
    }

    @Override
    public ParametersDescription getParametersDescription() {
        ParametersDescription desc = new ParametersDescription(this);
        desc.add(PRESERVESPACEBYDEFAULT, "Preserve Whitespace When Reading XLIFF", "Merging preserves the original whitespace in the XLIFF file");
        return desc;
    }

    @Override
    public EditorDescription createEditorDescription(ParametersDescription parametersDescription) {
        EditorDescription desc = new EditorDescription("OriginalDocumentXliffMergerStep Parameters", true, false);
        desc.addCheckboxPart(parametersDescription.get(PRESERVESPACEBYDEFAULT));
        return desc;
    }
}
