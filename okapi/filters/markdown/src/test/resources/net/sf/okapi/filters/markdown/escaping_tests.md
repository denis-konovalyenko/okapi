
## **Escape In Headers \(**[**View on GitLab**](https://gitlab.com/okapiframework/okapi)**\)**

Escape things \(that we care about\) in regular\_text.

Escape HTML entities like &amp;, &mdash;, &lt;, and &gt;, but only if configured to! leave these guys alone: @£¡.

We should disable escaping inside inline codes: `dontEscapeTheseParens(please)`.
