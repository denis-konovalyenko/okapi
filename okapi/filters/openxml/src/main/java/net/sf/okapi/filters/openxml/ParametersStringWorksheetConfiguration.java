/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ParametersString;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

final class ParametersStringWorksheetConfiguration implements WorksheetConfiguration {
    static final String DELIMITER = ",";
    private static final String DELIMITING_EXPRESSION = "\\s*" + DELIMITER + "\\s*";
    static final String NAME_PATTERN = "namePattern";
    static final String SOURCE_COLUMNS = "sourceColumns";
    static final String TARGET_COLUMNS = "targetColumns";
    static final String EXCLUDED_ROWS = "excludedRows";
    static final String EXCLUDED_COLUMNS = "excludedColumns";
    static final String METADATA_ROWS = "metadataRows";
    static final String METADATA_COLUMNS = "metadataColumns";
    private static final String EMPTY = "";
    private static final String DEFAULT_NAME_PATTERN = EMPTY;
    private static final String DEFAULT_SOURCE_COLUMNS = EMPTY;
    private static final String DEFAULT_TARGET_COLUMNS = EMPTY;
    private static final String DEFAULT_EXCLUDED_ROWS = EMPTY;
    private static final String DEFAULT_EXCLUDED_COLUMNS = EMPTY;
    private static final String DEFAULT_METADATA_ROWS = EMPTY;
    private static final String DEFAULT_METADATA_COLUMNS = EMPTY;

    private final ParametersString parametersString;

    private WorksheetConfiguration worksheetConfiguration;
    private boolean read;

    ParametersStringWorksheetConfiguration(final ParametersString parametersString) {
        this.parametersString = parametersString;
    }

    @Override
    public boolean matches(final String worksheetName) {
        fromParametersString();
        return this.worksheetConfiguration.matches(worksheetName);
    }

    @Override
    public Set<String> sourceColumns() {
        fromParametersString();
        return this.worksheetConfiguration.sourceColumns();
    }

    @Override
    public Set<String> targetColumns() {
        fromParametersString();
        return this.worksheetConfiguration.targetColumns();
    }

    @Override
    public Set<Integer> excludedRows() {
        fromParametersString();
        return this.worksheetConfiguration.excludedRows();
    }

    @Override
    public Set<String> excludedColumns() {
        fromParametersString();
        return this.worksheetConfiguration.excludedColumns();
    }

    @Override
    public Set<Integer> metadataRows() {
        fromParametersString();
        return this.worksheetConfiguration.metadataRows();
    }

    @Override
    public Set<String> metadataColumns() {
        fromParametersString();
        return this.worksheetConfiguration.metadataColumns();
    }

    /**
     * Reads worksheet configuration values from a parameters string.
     * <p>
     * The following values format is supported:
     * [namePattern]
     * [excludedRows]
     * [excludedColumns]
     * [metadataRows]
     * [metadataColumns]
     * <p>
     * The name pattern can be any supported regular expression.
     * <p>
     * The excluded rows can be a set of integer values delimited by
     * {@link ParametersStringWorksheetConfiguration#DELIMITER}.
     * <p>
     * The excluded columns can be a set of string values delimited by
     * {@link ParametersStringWorksheetConfiguration#DELIMITER}.
     */
    private void fromParametersString() {
        if (!this.read) {
            this.worksheetConfiguration = new WorksheetConfiguration.Default(
                this.parametersString.getString(
                    ParametersStringWorksheetConfiguration.NAME_PATTERN,
                    ParametersStringWorksheetConfiguration.DEFAULT_NAME_PATTERN
                ),
                columnsFor(
                    ParametersStringWorksheetConfiguration.SOURCE_COLUMNS,
                    ParametersStringWorksheetConfiguration.DEFAULT_SOURCE_COLUMNS
                ),
                columnsFor(
                    ParametersStringWorksheetConfiguration.TARGET_COLUMNS,
                    ParametersStringWorksheetConfiguration.DEFAULT_TARGET_COLUMNS
                ),
                rowsFor(
                    ParametersStringWorksheetConfiguration.EXCLUDED_ROWS,
                    ParametersStringWorksheetConfiguration.DEFAULT_EXCLUDED_ROWS
                ),
                columnsFor(
                    ParametersStringWorksheetConfiguration.EXCLUDED_COLUMNS,
                    ParametersStringWorksheetConfiguration.DEFAULT_EXCLUDED_COLUMNS
                ),
                rowsFor(
                    ParametersStringWorksheetConfiguration.METADATA_ROWS,
                    ParametersStringWorksheetConfiguration.DEFAULT_METADATA_ROWS
                ),
                columnsFor(
                    ParametersStringWorksheetConfiguration.METADATA_COLUMNS,
                    ParametersStringWorksheetConfiguration.DEFAULT_METADATA_COLUMNS
                )
            );
            this.read = true;
        }
    }

    private List<Integer> rowsFor(final String name, final String defaultValue) {
        return Arrays
                .stream(parametersFor(name, defaultValue))
                .filter(s-> !s.isEmpty())
                .map(Integer::parseUnsignedInt)
                .collect(Collectors.toList());
    }

    private List<String> columnsFor(final String name, final String defaultValue) {
        return Arrays
                .stream(parametersFor(name, defaultValue))
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }

    private String[] parametersFor(final String name, final String defaultValue) {
        return this.parametersString.getString(name, defaultValue)
                .split(DELIMITING_EXPRESSION);
    }

    @Override
    public <T> T writtenTo(final Output<T> output) {
        fromParametersString();
        return this.worksheetConfiguration.writtenTo(output);
    }
}
