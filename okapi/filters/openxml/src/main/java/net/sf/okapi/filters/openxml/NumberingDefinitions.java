/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

interface NumberingDefinitions {
    String NAME = "numbering";
    List<XMLEvent> startEvents();
    Iterator<NumberingDefinition> abstractNumberingDefinitionsIterator();
    Iterator<NumberingDefinition> numberingDefinitionsIterator();
    List<XMLEvent> endEvents();
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;
    void markReferencedFrom(final NumberingProperties numberingProperties);

    final class Empty implements NumberingDefinitions {
        @Override
        public List<XMLEvent> startEvents() {
            return Collections.emptyList();
        }

        @Override
        public Iterator<NumberingDefinition> abstractNumberingDefinitionsIterator() {
            return Collections.emptyIterator();
        }

        @Override
        public Iterator<NumberingDefinition> numberingDefinitionsIterator() {
            return Collections.emptyIterator();
        }

        @Override
        public List<XMLEvent> endEvents() {
            return Collections.emptyList();
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
        }

        @Override
        public void markReferencedFrom(final NumberingProperties numberingProperties) {
        }
    }

    final class Default implements NumberingDefinitions {
        private static final String NUM_PIC_BULLET = "numPicBullet";
        private static final String NUM_ID_MAC_AT_CLEANUP = "numIdMacAtCleanup";
        private final List<XMLEvent> eventsBeforeNumberingDefinitions;
        private final Map<String, NumberingDefinition> abstractNumberingDefinitions;
        private final Map<String, NumberingDefinition> numberingDefinitions;
        private final List<XMLEvent> eventsAfterNumberingDefinitions;

        Default() {
            this(
                new LinkedList<>(),
                new LinkedHashMap<>(),
                new LinkedHashMap<>(),
                new LinkedList<>()
            );
        }

        Default(
            List<XMLEvent> eventsBeforeNumberingDefinitions,
            final Map<String, NumberingDefinition> abstractNumberingDefinitions,
            final Map<String, NumberingDefinition> numberingDefinitions,
            final List<XMLEvent> eventsAfterNumberingDefinitions
        ) {
            this.eventsBeforeNumberingDefinitions = eventsBeforeNumberingDefinitions;
            this.abstractNumberingDefinitions = abstractNumberingDefinitions;
            this.numberingDefinitions = numberingDefinitions;
            this.eventsAfterNumberingDefinitions = eventsAfterNumberingDefinitions;
        }

        @Override
        public List<XMLEvent> startEvents() {
            return this.eventsBeforeNumberingDefinitions;
        }

        @Override
        public Iterator<NumberingDefinition> abstractNumberingDefinitionsIterator() {
            return this.abstractNumberingDefinitions.values().iterator();
        }

        @Override
        public Iterator<NumberingDefinition> numberingDefinitionsIterator() {
            return this.numberingDefinitions.values().iterator();
        }

        @Override
        public List<XMLEvent> endEvents() {
            return this.eventsAfterNumberingDefinitions;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartDocument()) {
                    this.eventsBeforeNumberingDefinitions.add(e);
                } else if (e.isStartElement()) {
                    final StartElement el = e.asStartElement();
                    if (NumberingDefinitions.NAME.equals(el.getName().getLocalPart())) {
                        this.eventsBeforeNumberingDefinitions.add(e);
                    } else if (NUM_PIC_BULLET.equals(el.getName().getLocalPart())) {
                        this.eventsBeforeNumberingDefinitions.addAll(
                            XMLEventHelpers.eventsFor(e.asStartElement(), reader)
                        );
                    } else if (NumberingDefinition.Abstract.NAME.equals(el.getName().getLocalPart())) {
                        final NumberingDefinition nd = new NumberingDefinition.Abstract(el);
                        nd.readWith(reader);
                        this.abstractNumberingDefinitions.put(nd.id(), nd);
                    } else if (NumberingDefinition.Instance.NAME.equals(el.getName().getLocalPart())) {
                        final NumberingDefinition nd = new NumberingDefinition.Instance(el, this.abstractNumberingDefinitions);
                        nd.readWith(reader);
                        this.numberingDefinitions.put(nd.id(), nd);
                    } else if (NUM_ID_MAC_AT_CLEANUP.equals(el.getName().getLocalPart())) {
                        this.eventsAfterNumberingDefinitions.addAll(
                            XMLEventHelpers.eventsFor(e.asStartElement(), reader)
                        );
                    }
                } else if (e.isEndElement()) {
                    final EndElement el = e.asEndElement();
                    if (NumberingDefinitions.NAME.equals(el.getName().getLocalPart())) {
                        this.eventsAfterNumberingDefinitions.add(e);
                    }
                } else if (e.isEndDocument()) {
                    this.eventsAfterNumberingDefinitions.add(e);
                }
            }
        }

        @Override
        public void markReferencedFrom(final NumberingProperties numberingProperties) {
            if (this.numberingDefinitions.containsKey(numberingProperties.numberingId())) {
                final NumberingDefinition nd = this.numberingDefinitions.get(numberingProperties.numberingId());
                nd.referencedWith(numberingProperties.numberingLevelId());
            }
        }
    }
}

