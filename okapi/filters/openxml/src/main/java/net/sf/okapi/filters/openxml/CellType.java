/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.Attribute;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

enum CellType {
    BOOLEAN("b"),
    DATE("d"),
    ERROR("e"),
    NUMBER("n"),
    STRING("str"),
    INLINE_STRING("inlineStr"),
    SHARED_STRING("s");

    private static final Map<String, CellType> stringLookup = Arrays.stream(CellType.values()).collect(Collectors.toMap(
            c -> c.value,
            c -> c
    ));
    private final String value;

    CellType(final String value) {
        this.value = value;
    }

    static CellType from(final Attribute attribute) {
        if (null == attribute) {
            return NUMBER;
        }
        return CellType.from(attribute.getValue());
    }

    static CellType from(final String string) {
        return stringLookup.getOrDefault(string, NUMBER);
    }

    @Override
    public String toString() {
        return value;
    }
}
