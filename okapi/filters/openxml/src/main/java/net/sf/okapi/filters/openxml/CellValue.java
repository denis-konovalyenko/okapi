/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

interface CellValue {
    String NAME = "v";
    CellValue copiedWithAdjusted(final DifferentialFormat.Combined combinedFormat);
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    void update(final Characters characters);
    void updateFormer(final Characters characters);
    void updateFormerCharacters();
    String asFormattedString();
    int asInteger();
    int asFormerInteger();
    Markup asMarkup();

    class Default implements CellValue {
        private static final String EMPTY = "";
        private final XMLEventFactory eventFactory;
        private final boolean date1904;
        private final CellType type;
        private final DifferentialFormat.Combined combinedFormat;
        private final StartElement startElement;
        private final List<Characters> characters;
        private final List<Characters> formerCharacters;
        private EndElement endElement;

        Default(
            final XMLEventFactory eventFactory,
            final boolean date1904,
            final CellType type,
            final DifferentialFormat.Combined combinedFormat,
            final StartElement startElement
        ) {
            this(
                eventFactory,
                date1904,
                type,
                combinedFormat,
                startElement,
                new ArrayList<>(),
                new ArrayList<>()
            );
        }

        Default(
            final XMLEventFactory eventFactory,
            final boolean date1904,
            final CellType type,
            final DifferentialFormat.Combined combinedFormat,
            final StartElement startElement,
            final List<Characters> characters,
            final List<Characters> formerCharacters
        ) {
            this.eventFactory = eventFactory;
            this.date1904 = date1904;
            this.type = type;
            this.combinedFormat = combinedFormat;
            this.startElement = startElement;
            this.characters = characters;
            this.formerCharacters = formerCharacters;
        }

        @Override
        public CellValue copiedWithAdjusted(final DifferentialFormat.Combined combinedFormat) {
            return new Default(
                this.eventFactory,
                this.date1904,
                this.type,
                combinedFormat,
                this.startElement,
                new ArrayList<>(this.characters),
                new ArrayList<>(this.formerCharacters)
            );
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isCharacters()) {
                    continue;
                }
                this.characters.add(e.asCharacters());
            }
        }

        @Override
        public void update(final Characters characters) {
            this.formerCharacters.clear();
            this.formerCharacters.addAll(this.characters);
            this.characters.clear();
            this.characters.add(characters);
        }

        @Override
        public void updateFormer(final Characters characters) {
            this.formerCharacters.clear();
            this.formerCharacters.add(characters);
        }

        @Override
        public void updateFormerCharacters() {
            this.formerCharacters.clear();
            this.formerCharacters.addAll(this.characters);
        }

        @Override
        public String asFormattedString() {
            final String s = stringFor(this.characters);
            switch (this.type) {
                case BOOLEAN:
                    return XMLEventHelpers.booleanAttributeTrueValues().contains(s)
                        ? XMLEventHelpers.BooleanAttributeValue.TRUE_STRING.toString()
                        : XMLEventHelpers.BooleanAttributeValue.FALSE_STRING.toString();
                case ERROR:
                    return EMPTY;
                case NUMBER:
                    return new ExcelNumber.Default(s, this.date1904).formattedWith(this.combinedFormat.numberFormat());
                default:
                    return s;
            }
        }

        private static String stringFor(final List<Characters> characters) {
            final String s;
            if (characters.size() == 1) {
                s = characters.get(0).getData().trim();
            } else {
                s = characters.stream()
                        .map(Characters::getData)
                        .collect(Collectors.joining())
                        .trim();
            }
            return s;
        }

        @Override
        public int asInteger() {
            return integerFor(this.characters);
        }

        @Override
        public int asFormerInteger() {
            return integerFor(this.formerCharacters);
        }

        private static int integerFor(final List<Characters> charactersList) {
            return integerFor(stringFor(charactersList));
        }

        private static int integerFor(final String value) {
            try {
                if (value.contains(".")) {
                    return Integer.parseUnsignedInt(value.substring(0, value.indexOf(".")));
                }
                return Integer.parseUnsignedInt(value);
            } catch (NumberFormatException e) {
                throw new IllegalStateException("Unexpected string: ".concat(value));
            }
        }

        @Override
        public Markup asMarkup() {
            final MarkupBuilder mb = new MarkupBuilder(
                new Markup.General(
                    new ArrayList<>(1)
                ),
                new ArrayList<>(2 + this.characters.size())
            );
            mb.add(this.startElement);
            for (Characters chars : this.characters) {
                mb.add(chars);
            }
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName().getPrefix(),
                    this.startElement.getName().getNamespaceURI(),
                    CellValue.NAME
                );
            }
            return this.endElement;
        }
    }
}
