/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.LinkedList;
import java.util.List;

interface NumberingText {
    String value();
    StartElement startElement();
    EndElement endElement();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    void markAsReferenced();
    boolean referenced();
    List<XMLEvent> asEvents();

    final class Default implements NumberingText {
        private static final String VAL = "val";
        private final StartElement startElement;
        private EndElement endElement;
        private String value;
        private boolean referenced;

        Default(final StartElement startElement) {
            this.startElement = startElement;
        }

        @Override
        public String value() {
            if (null == this.value) {
                this.value = this.startElement.getAttributeByName(
                    new QName(
                        this.startElement.getName().getNamespaceURI(),
                        Default.VAL,
                        this.startElement.getName().getPrefix()
                    )
                ).getValue();
            }
            return this.value;
        }

        @Override
        public StartElement startElement() {
            return this.startElement;
        }

        @Override
        public EndElement endElement() {
            return this.endElement;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.endElement = reader.nextTag().asEndElement();
        }

        @Override
        public void markAsReferenced() {
            this.referenced = true;
        }

        @Override
        public boolean referenced() {
            return this.referenced;
        }

        @Override
        public List<XMLEvent> asEvents() {
            final List<XMLEvent> events = new LinkedList<>();
            events.add(this.startElement);
            events.add(this.endElement);
            return events;
        }
    }
}
