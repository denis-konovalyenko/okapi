/*
 * =============================================================================
 * Copyright (C) 2010-2024 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.util.HashSet;
import java.util.Set;

interface SharedStringsFragments {
    boolean stringItemFormattedInlineAt(final int index);
    void addStringItemFormattedInlineAt(final int index);
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;

    final class Default implements SharedStringsFragments {
        private static final String R = "r";
        private final Set<Integer> formattedInlineIndexes;

        Default() {
            this(new HashSet<>());
        }

        Default(final Set<Integer> formattedInlineIndexes) {
            this.formattedInlineIndexes = formattedInlineIndexes;
        }

        @Override
        public boolean stringItemFormattedInlineAt(final int index) {
            return this.formattedInlineIndexes.contains(index);
        }

        @Override
        public void addStringItemFormattedInlineAt(final int index) {
            this.formattedInlineIndexes.add(index);
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            int index = 0;
            boolean formattedInline = false;
            while (eventReader.hasNext()) {
                final XMLEvent e = eventReader.nextEvent();
                if (e.isStartElement() && R.equals(e.asStartElement().getName().getLocalPart())) {
                    formattedInline = true;
                } else if (e.isEndElement() && SharedStrings.SI.equals(e.asEndElement().getName().getLocalPart())) {
                    if (formattedInline) {
                        this.formattedInlineIndexes.add(index);
                    }
                    index++;
                    formattedInline = false;
                }
            }
        }
    }
}
