/*===========================================================================
  Copyright (C) 2019 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================*/

package net.sf.okapi.filters.xliff2;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiMergeException;
import net.sf.okapi.common.filters.SubFilterSkeletonWriter;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.common.xliff2.XLIFF2PropertyStrings;
import net.sf.okapi.lib.xliff2.writer.XLIFFWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Stack;

class SubFilterInfo {
    private final SubFilterSkeletonWriter subfilter;
    private final ITextUnit parentTextUnit;

    public SubFilterInfo(SubFilterSkeletonWriter subfilter, ITextUnit parentTextUnit) {
        this.subfilter = subfilter;
        this.parentTextUnit = parentTextUnit;
    }

    public SubFilterSkeletonWriter getSubfilter() {
        return subfilter;
    }

    public ITextUnit getParentTextUnit() {
        return parentTextUnit;
    }
}

/**
 * The XLIFF2FilterWriter is essentially wrapper for the {@link XLIFFWriter}, similar to the {@link XLIFF2Filter}.
 * <p>
 * When developing for this, no manual building of XML Strings should be handled by this. Instead, that should all be
 * handled by the {@link XLIFFWriter}.
 */
public class XLIFF2FilterWriter implements IFilterWriter {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    private Output outputChoice;
    private File outputFile;
    private OutputStreamWriter writer;
    private Charset encoding;
    private LocaleId sourceLocale;
    private LocaleId targetLocale;

    private XLIFFWriter xliffToolkitWriter;
    private final OkpToX2Converter converter;

    private final EncoderManager encoderManager;
    private final boolean forceUniqueIds;
    private final boolean writeOriginalData;
    private Stack<SubFilterInfo> subFilters;
    private Parameters parameters;

    // Easier to keep track of where the output is going than using a bunch of null checks
    enum Output {NONE, PATH, STREAM}

    public XLIFF2FilterWriter(Parameters parameters, EncoderManager encoderManager) {
        this.converter = new OkpToX2Converter();
        this.outputChoice = Output.NONE;
        this.outputFile = null;
        this.encoding = StandardCharsets.UTF_8;
        this.encoderManager = encoderManager;
        this.parameters = parameters;
        this.forceUniqueIds = parameters.getForceUniqueIds();
        this.writeOriginalData = parameters.getWriteOriginalData();
        this.subFilters = new Stack<>();
    }

    @Override
    public String getName() {
        return "XLIFF2FilterWriter";
    }

    @Override
    public void setOptions(LocaleId locale, String defaultEncoding) {
        targetLocale = locale;
        encoding = Charset.forName(defaultEncoding);
    }

    @Override
    public void setOutput(String path) {
        close();
        outputFile = new File(path);
        outputChoice = Output.PATH;
    }

    @Override
    public void setOutput(OutputStream output) {
        close();
        writer = new OutputStreamWriter(output, encoding);
        outputChoice = Output.STREAM;
    }

    protected void processStartDocument(StartDocument startDoc) {
        initializeWriter(startDoc.getLocale());
        converter.startDocument(startDoc).forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processEndDocument(Ending ending) {
        converter.endDocument(ending).forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processStartSubDocument(StartSubDocument startSubDoc) {
        if (!subFilters.isEmpty()) {
            subFilters.peek().getSubfilter().processStartSubDocument(startSubDoc);
            return;
        }
        converter.startSubDocument(startSubDoc).forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processEndSubDocument(Ending ending) {
        if (!subFilters.isEmpty()) {
            subFilters.peek().getSubfilter().processEndSubDocument(ending);
            return;
        }
        converter.endSubDocument().forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processStartGroup(StartGroup startGroup) {
        if (!subFilters.isEmpty()) {
            subFilters.peek().getSubfilter().processStartGroup(startGroup);
            return;
        }
        converter.startGroup(startGroup).forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processEndGroup(Ending endGroup) {
        if (!subFilters.isEmpty()) {
            subFilters.peek().getSubfilter().processEndGroup(endGroup);
            return;
        }
        converter.endGroup(endGroup).forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processTextUnit(ITextUnit textUnit) {
        if (!subFilters.isEmpty()) {
            subFilters.peek().getSubfilter().processTextUnit(textUnit);
            return;
        }
        converter.textUnit(textUnit, targetLocale).forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processDocumentPart(DocumentPart documentPart) {
        if (!subFilters.isEmpty()) {
            subFilters.peek().getSubfilter().processDocumentPart(documentPart);
            return;
        }
        converter.documentPart(documentPart).forEach(xliffToolkitWriter::writeEvent);
    }

    protected void processStartSubfilter(StartSubfilter startSubfilter) {
        SubFilterSkeletonWriter subFilterSkeletonWriter = new SubFilterSkeletonWriter(startSubfilter);
        // setOptions calls subFilterSkeletonWriter.processStartDocument(startSubfilter)
        subFilterSkeletonWriter.setOptions(targetLocale, encoding.name(), startSubfilter);
        subFilters.push(new SubFilterInfo(subFilterSkeletonWriter, startSubfilter.getParentTextUnit()));
    }

    protected void processEndSubfilter(EndSubfilter ending) {
        subFilters.peek().getSubfilter().processEndDocument(ending);
        // when we write out to xliff2 chars will be escaped - no need to run parent encoder
        String targetText = subFilters.peek().getSubfilter().getOutput();

        // take the original parent text unit and update the target output
        ITextUnit parentTextUnit = subFilters.peek().getParentTextUnit();
        if (overWriteTarget(parentTextUnit)) {
            TextContainer targetContainer = new TextContainer(targetText);
            // source and target segment id's must match
            targetContainer.getFirstSegment().setId(parentTextUnit.getSource().getFirstSegment().getId());
            targetContainer.getFirstSegment().setOriginalId(parentTextUnit.getSource().getFirstSegment().getOriginalId());
            parentTextUnit.setTarget(targetLocale, targetContainer);
        }
        converter.textUnit(parentTextUnit, targetLocale).forEach(xliffToolkitWriter::writeEvent);
        subFilters.peek().getSubfilter().close();
        subFilters.pop();
    }

    private boolean overWriteTarget(ITextUnit parentTextUnit) {
        // if subfilterOverwriteTarget is set to true, we always overwrite the target no matter the state
        if (parameters != null && parameters.getSubfilterOverwriteTarget()) {
            return true;
        }

        // if target state is FINAL we don't want to overwrite the target, which is the default for the writer
        if (parentTextUnit.getSource().getFirstSegment().hasProperty(XLIFF2PropertyStrings.STATE) &&
                parentTextUnit.getSource().getFirstSegment().getProperty(XLIFF2PropertyStrings.STATE).getValue().equals("final")) {
            return false;
        }
        return true;
    }

    @Override
    public Event handleEvent(Event event) {
        if (outputChoice == Output.NONE) {
            throw new OkapiMergeException("Output has not been set. Use setOutput().");
        }
        switch (event.getEventType()) {
            case START_DOCUMENT:
                processStartDocument(event.getStartDocument());
                break;
            case END_DOCUMENT:
                processEndDocument(event.getEnding());
                close();
                break;
            case START_SUBDOCUMENT:
                processStartSubDocument(event.getStartSubDocument());
                break;
            case END_SUBDOCUMENT:
                processEndSubDocument(event.getEnding());
                break;
            case START_GROUP:
                processStartGroup(event.getStartGroup());
                break;
            case END_GROUP:
                processEndGroup(event.getEndGroup());
                break;
            case TEXT_UNIT:
                processTextUnit(event.getTextUnit());
                break;
            case DOCUMENT_PART:
                processDocumentPart(event.getDocumentPart());
                break;
            case START_SUBFILTER:
                processStartSubfilter(event.getStartSubfilter());
                break;
            case END_SUBFILTER:
                processEndSubfilter(event.getEndSubfilter());
                break;
            default:
                break;
        }

        return event;
    }

    @Override
    public void close() {
        if (xliffToolkitWriter != null) {
            xliffToolkitWriter.close();
            xliffToolkitWriter = null;
        }
        outputFile = null;
        outputChoice = Output.NONE;
    }

    @Override
    public IParameters getParameters() {
        return parameters;
    }

    @Override
    public void setParameters(IParameters params) {
        parameters = (Parameters) params;
    }

    @Override
    public void cancel() {
        close();
    }

    @Override
    public EncoderManager getEncoderManager() {
        return encoderManager;
    }

    @Override
    public ISkeletonWriter getSkeletonWriter() {
        // Not implemented yet
        return null;
    }

    /**
     * Initializes the writer with the specified source language and the set target locale. Because we can't
     * initialize the toolkit writer without a source locale, we have to wait until the Start Document event is
     * provided.
     *
     * @param sourceLang The source language derived from the XLIFF 2.0 file
     */
    void initializeWriter(LocaleId sourceLang) {
        sourceLocale = sourceLang;
        xliffToolkitWriter = new XLIFFWriter();
        final String target = targetLocale != null ? targetLocale.toString() : null;
        switch (outputChoice) {
            case PATH:
                xliffToolkitWriter.create(outputFile, sourceLocale.toString(), target);
                break;
            case STREAM:
                xliffToolkitWriter.create(writer, sourceLocale.toString(), target);
                break;
            case NONE:
                logger.error("No output set for XLIFF 2 to be written to");
                throw new RuntimeException("No output set for XLIFF 2 to be written to");
        }
        xliffToolkitWriter.setWithOriginalData(writeOriginalData);
        xliffToolkitWriter.setUseIndentation(true);
        xliffToolkitWriter.setForceUniqueIds(forceUniqueIds);
        xliffToolkitWriter.setWithOriginalData(writeOriginalData);
    }

    public LocaleId getSourceLocale() {
        return sourceLocale;
    }

    public LocaleId getTargetLocale() {
        return targetLocale;
    }

}
