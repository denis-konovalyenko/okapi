package net.sf.okapi.filters.ttml;

import net.sf.okapi.common.skeleton.GenericSkeletonPart;

public class TTMLSkeletonPart extends GenericSkeletonPart {
    public int startBeginTime;
    public int endBeginTime;
    public int startEndTime;
    public int endEndTime;

    private boolean isHeader;

    public TTMLSkeletonPart(String data, int startBeginTime, int endBeginTime, int startEndTime, int endEndTime) {
        super(data);
        this.startBeginTime = startBeginTime;
        this.endBeginTime = endBeginTime;
        this.startEndTime = startEndTime;
        this.endEndTime = endEndTime;
        this.isHeader = true;
    }

    public TTMLSkeletonPart(String data) {
        super(data);
        this.isHeader = false;
    }

    public String reconstruct(String beginTime, String endTime) {
        if (!isHeader) {
            throw new IllegalStateException("Cannot reconstruct non-header ttml skeleton part");
        }
        StringBuilder tmp = new StringBuilder();
        String data = toString();

        if (startBeginTime < startEndTime) {
            tmp.append(data.substring(0, startBeginTime));
            tmp.append(TTMLFilter.ATTR_TIME_BEGIN);
            tmp.append("=\"");
            tmp.append(beginTime);
            tmp.append("\"");
            tmp.append(data.substring(endBeginTime, startEndTime));
            tmp.append(TTMLFilter.ATTR_TIME_END);
            tmp.append("=\"");
            tmp.append(endTime);
            tmp.append("\"");
            tmp.append(data.substring(endEndTime));
        } else {
            tmp.append(data.substring(0, startEndTime));
            tmp.append(TTMLFilter.ATTR_TIME_END);
            tmp.append("=\"");
            tmp.append(beginTime);
            tmp.append("\"");
            tmp.append(data.substring(endEndTime, startBeginTime));
            tmp.append(TTMLFilter.ATTR_TIME_BEGIN);
            tmp.append("=\"");
            tmp.append(endTime);
            tmp.append("\"");
            tmp.append(data.substring(endBeginTime));
        }

        return tmp.toString();
    }

    public boolean isHeader() {
        return isHeader;
    }
}
