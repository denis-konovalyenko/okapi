/*
 * =============================================================================
 * Copyright (C) 2010-2020 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.xliff;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.filters.html.HtmlFilter;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
@RunWith(JUnit4.class)
public class PcdataSubfilteringTest {

    private final FilterConfigurationMapper filterConfigurationMapper;
    private final LocaleId sourceLocale;
    private final LocaleId targetLocale;

    private XLIFFFilter filter;
    private FileLocation fileLocation;

    public PcdataSubfilteringTest() {
        this.filterConfigurationMapper = new FilterConfigurationMapper();
        this.filterConfigurationMapper.addConfigurations(HtmlFilter.class.getName());
        this.sourceLocale = LocaleId.ENGLISH;
        this.targetLocale = LocaleId.FRENCH;
    }

    @Before
    public void setUp() {
        this.filter = new XLIFFFilter();
        this.filter.setFilterConfigurationMapper(filterConfigurationMapper);
        this.fileLocation = FileLocation.fromClass(this.getClass());
    }

    @Test
    public void subfilteredAsHtml() {
        final Parameters parameters = new Parameters();
        parameters.setPcdataSubfilter("okf_html");
        final List<Event> events = FilterTestDriver.getEvents(
                filter,
                new RawDocument(
                        fileLocation.in("/pcdatasubfiltering/test.xlf").asUri(),
                        XLIFFFilter.ENCODING.name(),
                        this.sourceLocale,
                        this.targetLocale
                ),
                parameters
        );
        final List<ITextUnit> tus = FilterTestDriver.filterTextUnits(events);
        Assertions.assertThat(tus).hasSize(1);
        Assertions.assertThat(tus.get(0).getSource().toString()).isEqualTo("PCDATA in source &");
        final DocumentPart subfilteredPart = FilterTestDriver.getDocumentPart(events, 2);
        Assertions.assertThat(((ITextUnit) subfilteredPart.getSkeleton().getParent()).getSource().toString()).isEqualTo("[#$1_ssf1@%source]");
        Assertions.assertThat(((ITextUnit) subfilteredPart.getSkeleton().getParent()).getTarget(this.targetLocale).toString()).isEqualTo("[#$1_ssf1@%target]");
        try {
            Assertions.assertThat(
                            FilterTestDriver.generateOutput(
                                    events,
                                    this.targetLocale,
                                    filter.createSkeletonWriter(),
                                    filter.getEncoderManager(),
                                    false
                            )
                    )
                    .isXmlEqualTo(
                            new String(
                                    Files.readAllBytes(
                                            this.fileLocation.in("/pcdatasubfiltering/test-gold.xlf").asPath()
                                    ),
                                    XLIFFFilter.ENCODING.name()
                            )
                    );
        } catch (final IOException e) {
            Assertions.fail("I/O exception: ", e);
        }
    }

    @Test
    public void subfilteredWithTargetsCopiedFromSourceAndTranslated() {
        final Parameters parameters = new Parameters();
        parameters.setPcdataSubfilter("okf_html");
        final List<Event> events = FilterTestDriver.getEvents(
                filter,
                new RawDocument(
                        fileLocation.in("/pcdatasubfiltering/test.xlf").asUri(),
                        XLIFFFilter.ENCODING.name(),
                        this.sourceLocale,
                        this.targetLocale
                ),
                parameters
        );
        final ITextUnit subfiltered = FilterTestDriver.getTextUnit(events, 1);
        subfiltered.setTarget(this.targetLocale, new TextContainer("Translated PCDATA"));
        try {
            Assertions.assertThat(
                            FilterTestDriver.generateOutput(
                                    events,
                                    this.targetLocale,
                                    filter.createSkeletonWriter(),
                                    filter.getEncoderManager(),
                                    false
                            )
                    )
                    .isXmlEqualTo(
                            new String(
                                    Files.readAllBytes(
                                            this.fileLocation.in("/pcdatasubfiltering/test-translated.xlf").asPath()
                                    ),
                                    XLIFFFilter.ENCODING.name()
                            )
                    );
        } catch (final IOException e) {
            Assertions.fail("I/O exception: ", e);
        }
    }
}
