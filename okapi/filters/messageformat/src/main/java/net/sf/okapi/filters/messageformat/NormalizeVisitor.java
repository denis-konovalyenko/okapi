package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessagePattern;
import com.ibm.icu.text.MessagePatternUtil;
import net.sf.okapi.common.IPredefinedLocales;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.IcuMessageEncoder;

import java.util.*;

/**
 * Currently not used, but may be useful in the future.
 */
public class NormalizeVisitor implements INodeVisitor {
    private final StringBuilder message;
    private final IcuMessageEncoder encoder;

    public NormalizeVisitor() {
        this.message = new StringBuilder();
        this.encoder = new IcuMessageEncoder();
    }

    public void normalize(List<MessagePatternUtil.MessageContentsNode> contents, List<MessagePatternUtil.MessageContentsNode> prefix, List<MessagePatternUtil.MessageContentsNode> postfix) {
        List<MessagePatternUtil.MessageContentsNode> newPrefix = new ArrayList<>(prefix);
        List<MessagePatternUtil.MessageContentsNode> newPostfix = new ArrayList<>(postfix);

        if (shouldNormalize(contents)) {
            for (MessagePatternUtil.MessageContentsNode contentsNode : contents) {
                if (shouldAppend(contentsNode)) {
                    newPrefix.add(contentsNode);
                } else {
                    break;
                }
            }

            ListIterator<MessagePatternUtil.MessageContentsNode> iterator = contents.listIterator(contents.size());
            while (iterator.hasPrevious()) {
                MessagePatternUtil.MessageContentsNode contentsNode = iterator.previous();
                if (shouldAppend(contentsNode)) {
                    newPostfix.add(contentsNode);
                } else {
                    break;
                }
                Collections.reverse(newPostfix);
            }

            // don't process nodes that are a prefix or postfix
            for (MessagePatternUtil.MessageContentsNode contentsNode : contents) {
                if (!shouldAppend(contentsNode)) {
                    visit(contentsNode, newPrefix, newPostfix);
                }
            }
        } else {
            for (MessagePatternUtil.MessageContentsNode contentsNode : contents) {
                visit(contentsNode, Collections.emptyList(), Collections.emptyList());
            }
        }
    }

    private boolean shouldNormalize(List<MessagePatternUtil.MessageContentsNode> contents) {
        for (MessagePatternUtil.MessageContentsNode contentsNode : contents) {
            if (shouldAppend(contentsNode)) {
                return contents.stream().anyMatch(n ->
                        n.getType() == MessagePatternUtil.MessageContentsNode.Type.ARG &&
                                ((MessagePatternUtil.ArgNode) n).getArgType() != MessagePattern.ArgType.NONE);
            }
        }
        return false;
    }

    private boolean shouldAppend(MessagePatternUtil.MessageContentsNode contentsNode) {
        return contentsNode.getType() == MessagePatternUtil.MessageContentsNode.Type.TEXT ||
                contentsNode.getType() == MessagePatternUtil.MessageContentsNode.Type.REPLACE_NUMBER ||
                (contentsNode.getType() == MessagePatternUtil.MessageContentsNode.Type.ARG &&
                        ((MessagePatternUtil.ArgNode) contentsNode).getArgType() == MessagePattern.ArgType.NONE);
    }

    public void visit(MessagePatternUtil.MessageContentsNode node, List<MessagePatternUtil.MessageContentsNode> prefix, List<MessagePatternUtil.MessageContentsNode> postfix) {
        switch (node.getType()) {
            case TEXT:
                visit((MessagePatternUtil.TextNode) node);
                break;
            case ARG:
                visit((MessagePatternUtil.ArgNode) node, prefix, postfix);
                break;
            case REPLACE_NUMBER:
                message.append("#");
                break;

        }
    }

    @Override
    public void visit(MessagePatternUtil.TextNode node) {
        String literalText = node.getText();
        long literalCharCount = literalText.chars().filter(
                (c -> c == '{' || c == '}' || c == '\\')).count();
        if (literalCharCount <= 0) {
            // If there are no literal characters return as-is
            message.append(literalText);
        } else {
            message.append(String.format("%s", encoder.encode(literalText, EncoderContext.TEXT)));
        }
    }

    public void visit(MessagePatternUtil.ArgNode node, List<MessagePatternUtil.MessageContentsNode> prefix, List<MessagePatternUtil.MessageContentsNode> postfix) {
        message.append("{");
        if (node.getArgType() == MessagePattern.ArgType.NONE) {
            message.append(String.format("%s", node.getName()));
        } else {
            message.append(String.format("%s, %s", node.getName(), node.getTypeName()));
            if (node.getSimpleStyle() != null) {
                message.append(", ");
                message.append(node.getSimpleStyle());
            }
        }

        MessagePatternUtil.ComplexArgStyleNode complexNode = node.getComplexStyle();
        if (complexNode != null) {
            visit(complexNode, prefix, postfix);
        }
        message.append("}");
    }


    public void visit(MessagePatternUtil.VariantNode node, List<MessagePatternUtil.MessageContentsNode> prefix, List<MessagePatternUtil.MessageContentsNode> postfix) {
        message.append(String.format(" %s ", node.getSelector()));
        message.append("{");
        message.append(getText(prefix));
        normalize(node.getMessage().getContents(), prefix, postfix);
        message.append(getText(postfix));
        message.append("}");
    }

    private String getText(List<MessagePatternUtil.MessageContentsNode> nodes) {
        if (nodes == null || nodes.isEmpty()) {
            return "";
        }

        StringBuilder text = new StringBuilder();
        for (MessagePatternUtil.MessageContentsNode node : nodes) {
            switch (node.getType()) {
                case TEXT:
                    text.append(((MessagePatternUtil.TextNode) node).getText());
                    break;
                case ARG:
                    text.append(node);
                    break;
                case REPLACE_NUMBER:
                    text.append("#");
                    break;
            }
        }

        return text.toString();
    }

    public void visit(MessagePatternUtil.ComplexArgStyleNode node, List<MessagePatternUtil.MessageContentsNode> prefix, List<MessagePatternUtil.MessageContentsNode> postfix) {
        if (node.getArgType() == MessagePattern.ArgType.CHOICE) {
            // FIXME: deprecated message format should we repair with select or plural?
        }

        if (node.hasExplicitOffset()) {
            message.append(String.format(", offset:%d", (int) node.getOffset()));
        } else {
            message.append(",");
        }
        List<MessagePatternUtil.VariantNode> variants = node.getVariants();
        for (MessagePatternUtil.VariantNode variant : variants) {
          visit(variant, prefix, postfix);
        }
    }

    @Override
    public String toString() {
        return message.toString();
    }
}
