/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.LocaleId;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class PluralRulesUtil {

    /**
     * Compares the plural forms of two locales and records the differences.
     *
     * @param sourceLocale the source locale
     * @param targetLocale the target locale
     * @param type         the plural type
     * @return the {@link PluralDiff} object
     */
    public static PluralDiff diffPluralRules(LocaleId sourceLocale, LocaleId targetLocale, com.ibm.icu.text.PluralRules.PluralType type) {
        com.ibm.icu.text.PluralRules sourceRules = com.ibm.icu.text.PluralRules.forLocale(sourceLocale.toIcuLocale(), type);
        com.ibm.icu.text.PluralRules targetRules = com.ibm.icu.text.PluralRules.forLocale(targetLocale == null ? sourceLocale.toIcuLocale() : targetLocale.toIcuLocale(), type);

        SortedSet<String> addedForms = targetRules.getKeywords().stream()
                .filter(keyword -> !sourceRules.getKeywords().contains(keyword))
                .collect(Collectors.toCollection(() -> new TreeSet<>(new PluralFormComparator())));

        SortedSet<String> subtractedForms = sourceRules.getKeywords().stream()
                .filter(keyword -> !targetRules.getKeywords().contains(keyword))
                .collect(Collectors.toCollection(() -> new TreeSet<>(new PluralFormComparator())));

        return new PluralDiff(sourceLocale, targetLocale, addedForms, subtractedForms);
    }
}
