package net.sf.okapi.filters.messageformat;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class PluralFormComparator implements Comparator<String> {
    private final Map<String, Integer> pluralFormRank;

    public PluralFormComparator() {
      pluralFormRank = new HashMap<>();
      pluralFormRank.put("=0", 1);
      pluralFormRank.put("zero", 2);
      pluralFormRank.put("one", 3);
      pluralFormRank.put("selectordinal0", 4);
      pluralFormRank.put("two", 5);
      pluralFormRank.put("selectordinal1", 6);
      pluralFormRank.put("few", 7);
      pluralFormRank.put("selectordinal2", 8);
      pluralFormRank.put("paucal", 9);
      pluralFormRank.put("many", 10);
      pluralFormRank.put("other", 11);
      pluralFormRank.put("selectordinal3", 12);
    }

    @Override
    public int compare(String variant1, String variant2) {
        if (variant1 == null) {
            return variant2 == null ? 0 : -1;
        }
        if (variant2 == null) {
            return 1;
        }
        // sort any unknown variants at the end
        if (!pluralFormRank.containsKey(variant1)) {
            return -1;
        }
        if (!pluralFormRank.containsKey(variant2)) {
            return -1;
        }
        return Integer.compare(pluralFormRank.get(variant1), pluralFormRank.get(variant2));
    }
}
