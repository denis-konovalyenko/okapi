/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.mif;

import net.sf.okapi.common.resource.CodeSimplifier;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragmentUtil;
import net.sf.okapi.common.resource.TextUnitUtil;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.SkeletonUtil;

interface TextUnitSimplification {
    void applyTo(final ITextUnit textUnit);

    class Default implements TextUnitSimplification {
        private final CodeSimplifier codeSimplifier;

        public Default(final CodeSimplifier codeSimplifier) {
            this.codeSimplifier = codeSimplifier;
        }

        @Override
        public void applyTo(final ITextUnit textUnit) {
            final TextFragment tf = textUnit.getSource().getUnSegmentedContentCopy();
            final TextFragment[] res = this.codeSimplifier.simplifyAll(tf, true, true);
            textUnit.setSourceContent(tf);
            if (null != res) {
                final GenericSkeleton original = TextUnitUtil.forceSkeleton(textUnit);
                final GenericSkeleton adjusted = new GenericSkeleton();
                adjusted.add(textFragmentAsString(res[0]));
                adjusted.addContentPlaceholder(textUnit);
                adjusted.add(textFragmentAsString(res[1]));
                final int index = SkeletonUtil.findTuRefInSkeleton(original);
                if (index != -1) {
                    SkeletonUtil.replaceSkeletonPart(original, index, adjusted);
                } else {
                    original.add(adjusted);
                }
            }
        }

        private static String textFragmentAsString(final TextFragment textFragment) {
            final String s;
            if (textFragment == null || textFragment.isEmpty()) {
                s = "";
            } else {
                s = TextFragmentUtil.toText(textFragment);
            }
            return s;
        }
    }
}
