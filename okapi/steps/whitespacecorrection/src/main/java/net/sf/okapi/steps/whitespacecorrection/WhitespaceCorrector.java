package net.sf.okapi.steps.whitespacecorrection;

import static net.sf.okapi.common.LocaleId.CHINA_CHINESE;
import static net.sf.okapi.common.LocaleId.JAPANESE;

import java.util.*;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.*;

public class WhitespaceCorrector {
    private static final char PARTSEP = '\uE091';

    public enum Whitespace {
		//vertical space
		LINE_FEED('\n'),
		LINE_TABULATION('\u000B'),
		FORM_FEED('\u000C'),
		CARRIAGE_RETURN('\r'),
		NEXT_LINE('\u0085'), 
		LINE_SEPARATOR('\u2028'),
		PARAGRAPH_SEPARATOR('\u2029'),
		
		//horizontal space
		CHARACTER_TABULATION('\u0009'),
		SPACE('\u0020'), 
		NO_BREAK_SPACE('\u00A0'), 
		EN_QUAD('\u2000'), 
		EM_QUAD('\u2001'), 
		EN_SPACE('\u2002'), 
		EM_SPACE('\u2003'), 
		THREE_PER_EM_SPACE('\u2004'), 
		FOUR_PER_EM_SPACER('\u2005'), 
		SIX_PER_EM_SPACE('\u2006'), 
		FIGURE_SPACE('\u2007'), 
		PUNCUATION_SPACE('\u2008'), 
		THIS_SPACE('\u2009'), 
		HAIR_SPACE('\u200A'), 
		NAORROW_NO_BREAK_SPACE('\u202F'), 
		MEDIUM_MATHEMATICAL_SPACE('\u205F'), 
		IDEOGRAPHIC_SPACE('\u3000'), 
		ZERO_WIDTH_SPACE('\u200B'), 
		ZERO_WIDTH_NON_BREAKING_SPACE('\uFEFF');
		
		private final char whitespace;
		
		Whitespace(char whitespace) {
			this.whitespace = whitespace;
		}

		public char getWhitespace() {
			return whitespace;
		}
	}
	
    public enum Punctuation {
        // U+3002 IDEOGRAPHIC FULL STOP, U+FF0E FULLWIDTH FULL STOP
        FULL_STOP('.', '\u3002', '\uFF0E'),
        // U+3001 IDEOGRAPHIC COMMA, U+FF0C FULLWIDTH COMMA
        COMMA(',', '\u3001', '\uFF0C'), 
        // U+FF01 FULLWIDTH EXCLAMATION MARK
        EXCLAMATION_MARK('!', '\uff01'),
        // U+FF1F FULLWIDTH QUESTION MARK
        QUESTION_MARK ('?', '\uff1f');

        private final char[] whitespaceNonAcceptingForm;
        private final char whitespaceAcceptingForm;

        Punctuation(char whitespaceAcceptingForm, char... whitespaceNonAcceptingForms) {
            this.whitespaceAcceptingForm = whitespaceAcceptingForm;
            this.whitespaceNonAcceptingForm = whitespaceNonAcceptingForms.clone();
        }

        public char getWhitespaceAcceptingForm() {
            return whitespaceAcceptingForm;
        }

        public char[] getWhitespaceNonAcceptingForm() {
        	return whitespaceNonAcceptingForm.clone();
        }
    }

    public static final Set<Whitespace> VERTICAL_WHITESPACE = Collections.unmodifiableSet(EnumSet.of(Whitespace.LINE_FEED,
    		Whitespace.LINE_TABULATION,
    		Whitespace.FORM_FEED,
    		Whitespace.CARRIAGE_RETURN,
    		Whitespace.NEXT_LINE, 
    		Whitespace.LINE_SEPARATOR,
    		Whitespace.PARAGRAPH_SEPARATOR));

    public static final Set<Whitespace> NONBREAKING_SPACES = Collections.unmodifiableSet(EnumSet.of(Whitespace.NO_BREAK_SPACE,
    		Whitespace.ZERO_WIDTH_NON_BREAKING_SPACE, 
    		Whitespace.NAORROW_NO_BREAK_SPACE));

    public static final Set<Whitespace> SPACE = Collections.unmodifiableSet(EnumSet.of(Whitespace.SPACE));

    public static final Set<Whitespace> ALL_WHITESPACE = Collections.unmodifiableSet(EnumSet.allOf(Whitespace.class));

    public static final Set<Whitespace> OTHER = Collections.unmodifiableSet(EnumSet.complementOf(EnumSet.of(Whitespace.LINE_FEED,
    		Whitespace.LINE_TABULATION,
    		Whitespace.FORM_FEED,
    		Whitespace.CARRIAGE_RETURN,
    		Whitespace.NEXT_LINE, 
    		Whitespace.LINE_SEPARATOR,
    		Whitespace.PARAGRAPH_SEPARATOR,
    		Whitespace.NO_BREAK_SPACE,
    		Whitespace.ZERO_WIDTH_NON_BREAKING_SPACE, 
    		Whitespace.NAORROW_NO_BREAK_SPACE,
    		Whitespace.SPACE,
    		Whitespace.CHARACTER_TABULATION
    		)));

    public static final Set<Whitespace> HORIZONTAL_TABS = Collections.unmodifiableSet(EnumSet.of(Whitespace.CHARACTER_TABULATION));
    
    protected static final char WHITESPACE = ' ';

    protected LocaleId sourceLocale;
    protected LocaleId targetLocale;
    protected Set<Punctuation> punctuation;
    protected Set<Whitespace> whitespace;

    public WhitespaceCorrector(LocaleId sourceLocale, LocaleId targetLocale, 
    		Set<Punctuation> punctuation, Set<Whitespace> whitespace) {
        this.sourceLocale = sourceLocale;
        this.targetLocale = targetLocale;
        this.punctuation = punctuation;
        this.whitespace = whitespace;
    }

    static boolean isSpaceDelimitedLanguage(LocaleId localeId) {
        return !JAPANESE.sameLanguageAs(localeId) && !CHINA_CHINESE.sameLanguageAs(localeId);
    }

    public ITextUnit correctWhitespace(ITextUnit tu) {
        // target TextContainer may be null if the translation unit has no target element
        if (tu.getTarget(targetLocale) != null) {
            if (isSpaceDelimitedLanguage(sourceLocale) && !isSpaceDelimitedLanguage(targetLocale)) {
                removeTrailingWhitespace(tu);
            } else if (!isSpaceDelimitedLanguage(sourceLocale) && isSpaceDelimitedLanguage(targetLocale)) {
                addTrailingWhitespace(tu);
            }
        }
        return tu;
    }

    private String segmentsToString(TextContainer tc) {
        StringBuilder tmp = new StringBuilder();
        for (TextPart part : tc) {
            // part to string
            tmp.append(part.getContent().getCodedText());
            // end of part to string: next part
            tmp.append(PARTSEP);
        }
        return tmp.toString();
    }

    private List<String> stringToSegments(String segments) {
        List<String> parts = new ArrayList<>();
        // split the string into segments producing the max number of elements even if empty
        // note this may produce more elements than the original, but we will just discard the last one
        Collections.addAll(parts, segments.split(String.valueOf(PARTSEP), -1));
        return parts;
    }

    protected void removeTrailingWhitespace(ITextUnit textUnit) {
        TextContainer targetTextContainer = textUnit.getTarget(targetLocale);
        /*
         * If whitespace trimming was enabled during segmentation, the
         * whitespace will be trapped in non-Segment TextParts.  So
         * we need to check everything in the container, not just the
         * results of tu.getTargetSegments();
         */
        String newSegments = findAndRemoveWhitespacesAfterPunctuation(segmentsToString(targetTextContainer));
        List<String> newParts = stringToSegments(newSegments);
        for (TextPart part : targetTextContainer.getParts()) {
            part.getContent().setCodedText(newParts.remove(0));
        }
    }



    protected void addTrailingWhitespace(ITextUnit textUnit) {
        TextContainer sourceTextContainer = textUnit.getSource();
        TextContainer targetTextContainer = textUnit.getTarget(targetLocale);

        Iterator<TextPart> sourceTextPartsIterator = sourceTextContainer.getParts().iterator();
        Iterator<TextPart> targetTextPartsIterator = targetTextContainer.getParts().iterator();

        while (sourceTextPartsIterator.hasNext() && targetTextPartsIterator.hasNext()) {
            TextPart sourceTextPart = sourceTextPartsIterator.next();
            TextPart targetTextPart = targetTextPartsIterator.next();

            String sourceText = sourceTextPart.getContent().getText();
            if (sourceText.isEmpty() || !isNonSpaceDelimitedPunctuation(lastChar(sourceText))) {
                // the text does not end with punctuation requiring conversion
                continue;
            }

            if (isWhitespace(lastChar(targetTextPart.getContent().getText()))) {
                // the whitespace is present at the end
                continue;
            }

            targetTextPart.getContent().append(WHITESPACE);
        }
    }

    protected boolean isWhitespace(char c) {
        for (Whitespace ws : whitespace) {
        	if (c == ws.whitespace) {
        		return true;
        	}
        }
        return false;
    }

    private char lastChar(String s) {
        return s.charAt(s.length() - 1);
    }

    protected boolean isSpaceDelimitedPunctuation(char c) {
        for (Punctuation p : punctuation) {
            if (c == p.whitespaceAcceptingForm) {
                return true;
            }
        }
        return false;
    }

    protected boolean isNonSpaceDelimitedPunctuation(char c) {
        for (Punctuation p : punctuation) {
            for (char form : p.whitespaceNonAcceptingForm) {
                if (form == c) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private String findAndRemoveWhitespacesAfterPunctuation(String segments) {
        StringBuilder newSegments = new StringBuilder(segments.length());
        for (int i = 0; i < segments.length(); i++) {
            newSegments.append(segments.charAt(i));
            if (isNonSpaceDelimitedPunctuation(segments.charAt(i))) {
                // remove all following whitespace, but ignore PARTSEP
                i++;
                while (i < segments.length() && ((isWhitespace(segments.charAt(i)) || segments.charAt(i) == PARTSEP))) {
                    // skip whitespace but keep the PARTSEP
                    if (segments.charAt(i) == PARTSEP) {
                        newSegments.append(segments.charAt(i));
                    }
                    i++;
                }
                // go back to capture the character after the whitespace
               i--;
            }
        }
        return newSegments.toString();
    }
}
