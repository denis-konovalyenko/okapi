package net.sf.okapi.common.filters;

import net.sf.okapi.common.*;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.SkeletonUtil;

import java.util.List;

public interface ISubFilter extends IFilter {
    /**
     * Create s a new subfilter. If filter is not a {@link ISubFilter}, a new {@link SubFilterWrapper} is created.
     * @param filter the filter to wrap.
     * @param parentEncoder the parent encoder.
     * @param sectionIndex  the section index of the parent filter.
     * @param parentId the parent id.
     * @param parentName the parent name.
     * @return the new {@link ISubFilter}.
     */
    static ISubFilter create(IFilter filter, IEncoder parentEncoder, int sectionIndex, String parentId, String parentName) {
        if (filter instanceof ISubFilter) {
            ((ISubFilter) filter).setParentId(parentId);
            ((ISubFilter) filter).setParentEncoder(parentEncoder);
            ((ISubFilter) filter).setParentName(parentName);
            ((ISubFilter) filter).setSectionIndex(sectionIndex);
            ((ISubFilter) filter).setParentType("");
            ((ISubFilter) filter).setConverter(new SubFilterEventConverter((ISubFilter) filter, parentEncoder));
            return (ISubFilter)filter;
        }
        return new SubFilterWrapper(filter, parentEncoder, sectionIndex, parentId, parentName);
    }

    static ISubFilter create(IFilter filter, IEncoder parentEncoder) {
        return create(filter, parentEncoder, 0, null, null);
    }

    static boolean resourceIdsMatch(String startSubfilterResourceId, String endSubfilterResourceId) {
        if (startSubfilterResourceId == null || endSubfilterResourceId == null) {
            return false;
        }
        int i = startSubfilterResourceId.lastIndexOf(IdGenerator.START_SUBFILTER);
        if (i != -1) {
            startSubfilterResourceId = startSubfilterResourceId.substring(0, i) +
                    startSubfilterResourceId.substring(i + IdGenerator.START_SUBFILTER.length());
        }
        i = endSubfilterResourceId.lastIndexOf(IdGenerator.END_SUBFILTER);
        if (i != -1) {
            endSubfilterResourceId = endSubfilterResourceId.substring(0, i) +
                    endSubfilterResourceId.substring(i + IdGenerator.END_SUBFILTER.length());
        }
        return startSubfilterResourceId.equals(endSubfilterResourceId);
    }


    default Event createRefEvent() {
        return createRefEvent(null, null);
    }

    default Event createRefEvent(IResource resource) {
        ISkeleton skel = resource.getSkeleton();
        if (skel instanceof GenericSkeleton) {
            GenericSkeleton[] parts = SkeletonUtil.splitSkeleton((GenericSkeleton) skel);
            return createRefEvent(parts[0], parts[1]);
        } else {
            if (skel != null)
                throw new OkapiException("Unknown skeleton type.");
            return createRefEvent();
        }
    }

    default Event createRefEvent(ISkeleton beforeSkeleton, ISkeleton afterSkeleton) {
        if (beforeSkeleton instanceof GenericSkeleton &&
                afterSkeleton instanceof GenericSkeleton) {
            DocumentPart dp = buildRefDP(
                    (GenericSkeleton) beforeSkeleton,
                    (GenericSkeleton) afterSkeleton);
            return new Event(EventType.DOCUMENT_PART, dp);
        } else {
            if (beforeSkeleton != null || afterSkeleton != null)
                throw new OkapiException("Unknown skeleton type.");
            DocumentPart dp = buildRefDP(null, null);
            return new Event(EventType.DOCUMENT_PART, dp);
        }
    }

    // We need a separate event to store a reference to the sub-filtered content,
    // and cannot always attach the reference to EndSubfilter's skeleton because the
    // content can also be accessed from a code in a TextFragment or skeleton.
    // We have 2 options to refer to the content: from an event and from a code in
    // a TextFragment or skeleton.
    // If none of these options is used, the content remains unreferenced, and
    // the parent filter's writer has no means to write it out.
    // Sub-filtered content is accessible only by a reference, so it's the parent
    // filter's responsibility to invoke creation of such reference.
     private DocumentPart buildRefDP(GenericSkeleton beforeSkeleton, GenericSkeleton afterSkeleton) {
        GenericSkeleton skel = new GenericSkeleton();
        DocumentPart dp = new DocumentPart(buildRefId(), false, skel);

        skel.add(beforeSkeleton);
        skel.addReference(getStartSubfilter());
        getStartSubfilter().setIsReferent(true);
        skel.add(afterSkeleton);

        dp.setName(buildRefName());
        dp.setType("ref-ssf");

        return dp;
    }

    private String buildStartSubFilterId(String originalResId) {
        return (originalResId != null) ?
                String.format("%s_%s", getParentId(), originalResId) :
                String.format("%s_%s%d", getParentId(), IdGenerator.START_SUBFILTER, getSectionIndex());
    }

    private String buildStartSubFilterName() {
        return IFilter.SUB_FILTER + getParentName();
    }

    private String buildEndSubFilterId(String originalResId) {
        return (originalResId != null) ?
                String.format("%s_%s", getParentId(), originalResId) :
                String.format("%s_%s%d", getParentId(), IdGenerator.END_SUBFILTER, getSectionIndex());
    }

    default String buildResourceId(String resId,
                                     Class<? extends IResource> resClass) {
        if (resClass == StartSubfilter.class)
            return buildStartSubFilterId(resId);

        else if (resClass == EndSubfilter.class)
            return buildEndSubFilterId(resId);

        else
            return String.format("%s_%s%d_%s", getParentId(),
                    IdGenerator.SUBFILTERED_EVENT, getSectionIndex(), resId);
    }

    default String buildResourceName(String resName,
                             boolean autoGenerated,
                             Class<? extends INameable> resClass) {
        if (resClass == StartSubfilter.class)
            return buildStartSubFilterName();
        else
            return autoGenerated ? String.format("%s_%s", getParentName(), resName) : resName;
    }

    private String buildRefId() {
        return String.format("ref-%s%s-%d", IFilter.SUB_FILTER, getParentId(), getSectionIndex());
    }

    private String buildRefName() {
        return "ref:" + getParentId();
    }

    /**
     * Get events by subfilter at once, without using open()/hasNext()/next()/close().
     *
     * @param tu the {@link ITextUnit} to retrieve events from.
     * @return a list of events created by the subfilter for a given {@link TextUnit} input.
     */
    List<Event> getEvents(ITextUnit tu, LocaleId sourceLocale, LocaleId targetLocale);

    /**
     * Get events by subfilter at once, without using open()/hasNext()/next()/close().
     *
     * @param input the {@link RawDocument} to retrieve events from.
     * @return a list of events created the this subfilter for a given RawDocument input.
     */
    List<Event> getEvents(RawDocument input);

    /**
     * Get the reference Code for the subfilter events.
     *
     * @return a {@link Code}
     */
    default Code createRefCode() {
        getStartSubfilter().setIsReferent(true);
        Code c = new Code(TextFragment.TagType.PLACEHOLDER, getStartSubfilter().getName(),
                TextFragment.makeRefMarker(getStartSubfilter().getId()));
        c.setReferenceFlag(true);
        return c;
    }

    default Code createSourceRefCode() {
        getStartSubfilter().setIsReferent(true);
        Code c = new Code(TextFragment.TagType.PLACEHOLDER, getStartSubfilter().getName(),
                TextFragment.makeRefMarker(getStartSubfilter().getId(), "source"));
        c.setReferenceFlag(true);
        return c;
    }

    default Code createTargetRefCode() {
        getStartSubfilter().setIsReferent(true);
        Code c = new Code(TextFragment.TagType.PLACEHOLDER, getStartSubfilter().getName(),
                TextFragment.makeRefMarker(getStartSubfilter().getId(), "target"));
        c.setReferenceFlag(true);
        return c;
    }

    StartSubfilter getStartSubfilter();

    String getParentName();

    int getSectionIndex();

    String getParentId();

    String getParentType();

    void setParentId(String parentId);

    void setParentEncoder(IEncoder parentEncoder);

    void setParentName(String parentName);

    void setSectionIndex(int sectionIndex);

    void setParentType(String parentType);

    void setStartSubfilter(StartSubfilter startSubfilter);

    void setEndSubfilter(EndSubfilter endSubfilter);

    void setConverter(SubFilterEventConverter converter);

    SubFilterEventConverter getConverter();
}
