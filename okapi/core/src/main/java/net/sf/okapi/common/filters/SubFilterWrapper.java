/*===========================================================================
  Copyright (C) 2008-2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Wrapper that converts any {@link IFilter} into a {@link ISubFilter} (a filter called from another {@link IFilter}).
 * Specific implementations can implement this class and override any needed methods to transform {@link Event}s
 * as they are produced.
 * This class should be used to wrap filters that use {@link GenericSkeleton} and its subclasses.
 * If a different type of skeleton is used or id/name generation logic should be changed, subclass this class.
 */
public class SubFilterWrapper extends AbstractSubFilter {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final IFilter filter;

    public SubFilterWrapper(IFilter filter, IEncoder parentEncoder, int sectionIndex, String parentId, String parentName) {
        super();
        filter.close();
        this.filter = filter;
        setParentId(parentId);
        setParentName(parentName);
        setSectionIndex(sectionIndex);
        setParentEncoder(parentEncoder);
        setConverter(new SubFilterEventConverter(this, parentEncoder));
    }

    public IFilter getFilter() {
        return filter;
    }

    @Override
    public String getName() {
        return filter.getName();
    }

    @Override
    public String getDisplayName() {
        return filter.getDisplayName();
    }

    @Override
    public void open(RawDocument input) {
       open(input, true);
    }

    @Override
    public void open(RawDocument input, boolean generateSkeleton) {
        getConverter().reset();
        filter.open(input, generateSkeleton);
    }

    @Override
    public void close() {
        filter.close();
        getConverter().reset();
    }

    @Override
    public boolean hasNext() {
        return filter.hasNext();
    }

    @Override
    public Event next() {
        return getConverter().convertEvent(filter.next());
    }

    @Override
    public void cancel() {
        filter.cancel();
    }

    @Override
    public IParameters getParameters() {
        return filter.getParameters();
    }

    @Override
    public void setParameters(IParameters params) {
        filter.setParameters(params);
    }

    @Override
    public ISkeletonWriter createSkeletonWriter() {
        return filter.createSkeletonWriter();
    }

    @Override
    public IFilterWriter createFilterWriter() {
        return filter.createFilterWriter();
    }

    @Override
    public EncoderManager getEncoderManager() {
        return filter.getEncoderManager();
    }

    @Override
    public String getMimeType() {
        return filter.getMimeType();
    }

    @Override
    public void setFilterConfigurationMapper(IFilterConfigurationMapper fcMapper) {
        super.setFilterConfigurationMapper(fcMapper);
        filter.setFilterConfigurationMapper(getFilterConfigurationMapper());
    }

    @Override
    public List<FilterConfiguration> getConfigurations() {
        return filter.getConfigurations();
    }
}
