/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common;

/**
 * An extensive list of {@link LocaleId}s predefined for easy access.
 *
 * @author hargraveje
 *
 */
public interface IPredefinedLocales {
	LocaleId Afrikaans = LocaleId.fromString("af_ZA");
	LocaleId Albanian = LocaleId.fromString("sq_AL");
	LocaleId Amharic = LocaleId.fromString("amh_ET");
	LocaleId Armenian = LocaleId.fromString("hy_AM");
	LocaleId Aymara = LocaleId.fromString("ayc_PE");

	LocaleId Basque = LocaleId.fromString("eu_ES");
	LocaleId Bikolano = LocaleId.fromString("bik_PH");
	LocaleId Bislama = LocaleId.fromString("bis_VU");
	LocaleId Bulgarian = LocaleId.fromString("bg_BG");
	LocaleId Burmese = LocaleId.fromString("mya_MM");

	LocaleId Cebuano = LocaleId.fromString("ceb_PH");
	LocaleId Chuukese = LocaleId.fromString("chk_FM");
	LocaleId Croatian = LocaleId.fromString("hr_HR");
	LocaleId Czech = LocaleId.fromString("cs_CZ");

	LocaleId Danish = LocaleId.fromString("da_DK");
	LocaleId Dutch = LocaleId.fromString("nl_NL");

	LocaleId Efik = LocaleId.fromString("efi_NG");
	LocaleId Estonian = LocaleId.fromString("et_EE");

	LocaleId Fante = LocaleId.fromString("fan_GH");
	LocaleId Fijian = LocaleId.fromString("fij_FJ");
	LocaleId Finnish = LocaleId.fromString("fi_FI");

	LocaleId Georgian = LocaleId.fromString("ka_GE");
	LocaleId Greek = LocaleId.fromString("el_GR");
	LocaleId Guarani = LocaleId.fromString("gug_PY");

	LocaleId Haitian = LocaleId.fromString("hat_HT");
	LocaleId Hiligaynon = LocaleId.fromString("hil_PH");
	LocaleId Hindi = LocaleId.fromString("hi_IN");
	LocaleId Hindi_Fiji = LocaleId.fromString("hin_FJ");
	LocaleId Hmong = LocaleId.fromString("mww_CN");
	LocaleId Hungarian = LocaleId.fromString("hu_HU");

	LocaleId Iban = LocaleId.fromString("iba_MY");
	LocaleId Icelandic = LocaleId.fromString("is_IS");
	LocaleId Igbo = LocaleId.fromString("ib_NG");
	LocaleId Ilocano = LocaleId.fromString("ilo_PH");
	LocaleId Indonesian = LocaleId.fromString("id_ID");

	LocaleId Kannada = LocaleId.fromString("kn_IN");
	LocaleId Kazakh = LocaleId.fromString("kk_KZ");
	LocaleId Kekchi = LocaleId.fromString("kek_GT");
	LocaleId Khmer = LocaleId.fromString("khm_KH");
	LocaleId Kirghiz = LocaleId.fromString("ky_KG");
	LocaleId Kiribati = LocaleId.fromString("gil_KI");
	LocaleId Korean = LocaleId.fromString("ko_KR");
	LocaleId Kosraean = LocaleId.fromString("kos_FM");

	LocaleId Lao = LocaleId.fromString("lo_LA");
	LocaleId Latvian = LocaleId.fromString("lv_LV");
	LocaleId Lingala = LocaleId.fromString("lin_CD");
	LocaleId Lithuanian = LocaleId.fromString("lt_LT");

	LocaleId Macedonian = LocaleId.fromString("mk_MK");
	LocaleId Malagasy = LocaleId.fromString("msh_MG");
	LocaleId Malay_Malaysia = LocaleId.fromString("ms_MY");
	LocaleId Malayalam = LocaleId.fromString("ml_IN");
	LocaleId Maltese = LocaleId.fromString("mt_MT");
	LocaleId Mam = LocaleId.fromString("mvc_GT");
	LocaleId Marshallese = LocaleId.fromString("mah_MH");
	LocaleId Mongolian = LocaleId.fromString("mn_MN");

	LocaleId Navajo = LocaleId.fromString("nav_US");
	LocaleId Neomelanesian = LocaleId.fromString("tpi_PG"); // Tok Pisin
	LocaleId Norwegian = LocaleId.fromString("no_NO");

	LocaleId Palauan = LocaleId.fromString("pau_PW");
	LocaleId Pohnpeian = LocaleId.fromString("pon_FM");
	LocaleId Polish = LocaleId.fromString("pl_PL");

	LocaleId Quechua = LocaleId.fromString("qul_BO"); // Bolivia
	LocaleId Quichua = LocaleId.fromString("qur_EC"); // Ecuador

	LocaleId Rarotongan = LocaleId.fromString("rar_CK");
	LocaleId Romanian = LocaleId.fromString("ro_RO");
	LocaleId Rwanda = LocaleId.fromString("kin_RW");

	LocaleId Samoan = LocaleId.fromString("smo_WS");
	LocaleId Serbian = LocaleId.fromString("sr_SP");
	LocaleId Shona = LocaleId.fromString("sna_ZW");
	LocaleId Slovak = LocaleId.fromString("sk_SK");
	LocaleId Slovenian = LocaleId.fromString("sl_SI");
	LocaleId South_Sotho = LocaleId.fromString("sot_LS"); // Sesotho
	LocaleId Swahili = LocaleId.fromString("sw_KE");
	LocaleId Swedish = LocaleId.fromString("sv_SE");

	LocaleId Tagalog = LocaleId.fromString("tgl_PH");
	LocaleId Tahitian = LocaleId.fromString("tah_PF");
	LocaleId Thai = LocaleId.fromString("th_TH");
	LocaleId Tongan = LocaleId.fromString("ton_TO");
	LocaleId Tswana = LocaleId.fromString("tsn_BW");
	LocaleId Turkish = LocaleId.fromString("tr_TR");

	LocaleId Twi = LocaleId.fromString("twi_GH");

	LocaleId Ukrainian = LocaleId.fromString("uk_UA");

	LocaleId Vietnamese = LocaleId.fromString("vi_VN");

	LocaleId Waray = LocaleId.fromString("war_PH"); // aka Samarnon, Warai

	LocaleId Xhosa = LocaleId.fromString("xho_ZA");

	LocaleId Yapese = LocaleId.fromString("yap_FM");
	LocaleId Yoruba = LocaleId.fromString("yor_NG");
	LocaleId Yupik = LocaleId.fromString("ess_US");

	LocaleId Zulu = LocaleId.fromString("zu_ZA");
}
