package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonSimplifier;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class SubFilterWrapperStep extends BasePipelineStep implements IPipelineStep {
    private final ISubFilter subFilter;
    private final IFilter primaryFilter;
    private int sectionIndex = 1;

    public SubFilterWrapperStep(IFilter filter, IFilter primaryFilter) {
        this.subFilter = ISubFilter.create(filter, primaryFilter.getEncoderManager());
        this.primaryFilter = primaryFilter;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String getDescription() {
        return "Wrapper around a SubFilter that produces flat results";
    }

    @Override
    public Stream<Event> handleStream(Event event) {
        try {
            if (event.isTextUnit()) {
                ITextUnit tu = event.getTextUnit();
                subFilter.setSectionIndex(sectionIndex++);
                // handle skeleton in TextUnit
                List<Event> allEvents = new ArrayList<>();
                for (Event tuEvent : processSkeleton(event.getTextUnit())) {
                    if (tuEvent.isTextUnit()) {
                        allEvents.addAll(subFilter.getEvents(tu, getSourceLocale(), getTargetLocale()));
                    } else {
                        allEvents.add(tuEvent);
                    }
                }
                return Stream.of(allEvents.toArray(new Event[0]));
            }
        } catch(Exception e) {
            throw new OkapiException(
                    String.format("Error in subfilter: %s processing %s", subFilter.getName(), event.toString()), e);
        } finally{
            subFilter.close();
        }

        return Stream.of(event);
    }

    private List<Event> processSkeleton(ITextUnit tu) {
        if (tu.getSkeleton() != null) {
            if (tu.getSkeleton() instanceof GenericSkeleton) {
                GenericSkeletonSimplifier rs = new GenericSkeletonSimplifier(primaryFilter.isMultilingual(),
                        (GenericSkeletonWriter) primaryFilter.createSkeletonWriter(), getTargetLocale());
                return rs.convertToList(new Event(EventType.TEXT_UNIT, tu));
            } else {
                return Collections.singletonList(new Event(EventType.TEXT_UNIT, tu));
            }
        }
        return Collections.singletonList(new Event(EventType.TEXT_UNIT, tu));
    }

    @Override
    public void close() {
        subFilter.close();
    }

    public ISubFilter getSubFilter() {
        return subFilter;
    }
}
