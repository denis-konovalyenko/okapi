package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.ttx.TTXFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class TtxXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_ttx";
	private static final String DIR_NAME = "/ttx/";
	private static final List<String> EXTENSIONS = Arrays.asList(".ttx");

	public TtxXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, LocaleId.EMPTY, TTXFilter::new);
	}

	@Test
	public void ttxXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(true, new FileComparator.XmlComparator());
	}
}
