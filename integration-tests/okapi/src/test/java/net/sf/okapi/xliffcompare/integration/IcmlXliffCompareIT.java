package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.icml.ICMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class IcmlXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_icml";
	private static final String DIR_NAME = "/icml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".icml", ".wcml");

	public IcmlXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, ICMLFilter::new);
	}

	@Test
	public void icmlXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
