/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications;

import org.slf4j.event.Level;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.helpers.MessageFormatter;

public class OkapiLogger extends MarkerIgnoringBase {
	private static final long serialVersionUID = 1L;
	private static IOkapiLoggerAppender appender;

	public OkapiLogger(String name) {
		this.name = name;
	}

	public static void setAppender(IOkapiLoggerAppender newAppender) {
		appender = newAppender;
	}

	@Override
	public boolean isTraceEnabled() {
		return isLevelEnabled(Level.TRACE);
	}

	@Override
	public void trace(String msg) {
		if (isTraceEnabled()) {
			handle_0ArgsCall(Level.TRACE, msg, /* Throwable */ null);
		}
	}

	@Override
	public void trace(String format, Object arg) {
		if (isTraceEnabled()) {
			handle_1ArgsCall(Level.TRACE, format, arg);
		}
	}

	@Override
	public void trace(String format, Object arg1, Object arg2) {
		if (isTraceEnabled()) {
			handle2ArgsCall(Level.TRACE, format, arg1, arg2);
		}
	}

	@Override
	public void trace(String format, Object... arguments) {
		if (isTraceEnabled()) {
			handleArgArrayCall(Level.TRACE, format, arguments);
		}
	}

	@Override
	public void trace(String msg, Throwable t) {
		if (isTraceEnabled()) {
			handle_0ArgsCall(Level.TRACE, msg, t);
		}
	}

	// Debug

	@Override
	public boolean isDebugEnabled() {
		return isLevelEnabled(Level.DEBUG);
	}

	@Override
	public void debug(String msg) {
		if (isDebugEnabled()) {
			handle_0ArgsCall(Level.DEBUG, msg, /* Throwable */ null);
		}
	}

	@Override
	public void debug(String format, Object arg) {
		if (isDebugEnabled()) {
			handle_1ArgsCall(Level.DEBUG, format, arg);
		}
	}

	@Override
	public void debug(String format, Object arg1, Object arg2) {
		if (isDebugEnabled()) {
			handle2ArgsCall(Level.DEBUG, format, arg1, arg2);
		}
	}

	@Override
	public void debug(String format, Object... arguments) {
		if (isDebugEnabled()) {
			handleArgArrayCall(Level.DEBUG, format, arguments);
		}
	}

	@Override
	public void debug(String msg, Throwable t) {
		if (isDebugEnabled()) {
			handle_0ArgsCall(Level.DEBUG, msg, t);
		}
	}

	// Info

	@Override
	public boolean isInfoEnabled() {
		return isLevelEnabled(Level.INFO);
	}

	@Override
	public void info(String msg) {
		if (isInfoEnabled()) {
			handle_0ArgsCall(Level.INFO, msg, /* Throwable */ null);
		}
	}

	@Override
	public void info(String format, Object arg) {
		if (isInfoEnabled()) {
			handle_1ArgsCall(Level.INFO, format, arg);
		}
	}

	@Override
	public void info(String format, Object arg1, Object arg2) {
		if (isInfoEnabled()) {
			handle2ArgsCall(Level.INFO, format, arg1, arg2);
		}
	}

	@Override
	public void info(String format, Object... arguments) {
		if (isInfoEnabled()) {
			handleArgArrayCall(Level.INFO, format, arguments);
		}
	}

	@Override
	public void info(String msg, Throwable t) {
		if (isInfoEnabled()) {
			handle_0ArgsCall(Level.INFO, msg, t);
		}
	}

	// Warn

	@Override
	public boolean isWarnEnabled() {
		return isLevelEnabled(Level.WARN);
	}

	@Override
	public void warn(String msg) {
		if (isWarnEnabled()) {
			handle_0ArgsCall(Level.WARN, msg, /* Throwable */ null);
		}
	}

	@Override
	public void warn(String format, Object arg) {
		if (isWarnEnabled()) {
			handle_1ArgsCall(Level.WARN, format, arg);
		}
	}

	@Override
	public void warn(String format, Object arg1, Object arg2) {
		if (isWarnEnabled()) {
			handle2ArgsCall(Level.WARN, format, arg1, arg2);
		}
	}

	@Override
	public void warn(String format, Object... arguments) {
		if (isWarnEnabled()) {
			handleArgArrayCall(Level.WARN, format, arguments);
		}
	}

	@Override
	public void warn(String msg, Throwable t) {
		if (isWarnEnabled()) {
			handle_0ArgsCall(Level.WARN, msg, t);
		}
	}

	// Error

	@Override
	public boolean isErrorEnabled() {
		return isLevelEnabled(Level.ERROR);
	}

	@Override
	public void error(String msg) {
		if (isErrorEnabled()) {
			handle_0ArgsCall(Level.ERROR, msg, /* Throwable */ null);
		}
	}

	@Override
	public void error(String format, Object arg) {
		if (isErrorEnabled()) {
			handle_1ArgsCall(Level.ERROR, format, arg);
		}
	}

	@Override
	public void error(String format, Object arg1, Object arg2) {
		if (isErrorEnabled()) {
			handle2ArgsCall(Level.ERROR, format, arg1, arg2);
		}
	}

	@Override
	public void error(String format, Object... arguments) {
		if (isErrorEnabled()) {
			handleArgArrayCall(Level.ERROR, format, arguments);
		}
	}

	@Override
	public void error(String msg, Throwable t) {
		if (isErrorEnabled()) {
			handle_0ArgsCall(Level.ERROR, msg, t);
		}
	}

	// Helper methods, inspired by what slf4j does in 2.0.0-alpha

	private boolean isLevelEnabled(Level level) {
		if (appender == null) // Can't log anyway
			return false;
		if (level == null) // Default to enable all logging
			return true;
		return level.toInt() >= appender.getLevel().toInt();
	}

	private void handle_0ArgsCall(Level level, String msg, Throwable t) {
		handleNormalizedLoggingCall(level, msg, null, t);
	}

	private void handle_1ArgsCall(Level level, String msg, Object arg1) {
		handleNormalizedLoggingCall(level, msg, new Object[] { arg1 }, null);
	}

	private void handle2ArgsCall(Level level, String msg, Object arg1, Object arg2) {
		if (arg2 instanceof Throwable) {
			handleNormalizedLoggingCall(level, msg, new Object[] { arg1 }, (Throwable) arg2);
		} else {
			handleNormalizedLoggingCall(level, msg, new Object[] { arg1, arg2 }, null);
		}
	}

	private void handleArgArrayCall(Level level, String msg, Object[] args) {
		Throwable throwableCandidate = MessageFormatter.getThrowableCandidate(args);
		if (throwableCandidate != null) {
			Object[] trimmedCopy = MessageFormatter.trimmedCopy(args);
			handleNormalizedLoggingCall(level, msg, trimmedCopy, throwableCandidate);
		} else {
			handleNormalizedLoggingCall(level, msg, args, null);
		}
	}

	private void handleNormalizedLoggingCall(Level level, String format, Object[] arguments, Throwable throwable) {
		// isLevelEnabled already checks the appender == null case

		FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
		if (ft.getThrowable() != null && throwable != null) {
			throw new IllegalArgumentException(
					"both last element in argument array and last argument are of type Throwable");
		}

		if (ft.getThrowable() != null) {
			throw new IllegalStateException("fix above code");
		}

		appender.log(name, level, ft.getMessage(), throwable);
	}
}
