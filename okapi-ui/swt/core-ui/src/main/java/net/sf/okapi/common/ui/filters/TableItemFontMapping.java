/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.common.ui.filters;

import net.sf.okapi.common.LocalePair;
import net.sf.okapi.common.filters.fontmappings.DefaultFontMapping;
import net.sf.okapi.common.filters.fontmappings.FontMapping;
import org.eclipse.swt.widgets.TableItem;

final class TableItemFontMapping implements FontMapping {
    private final TableItem tableItem;
    private FontMapping defaultFontMapping;
    private boolean read;

    TableItemFontMapping(final TableItem tableItem) {
        this.tableItem = tableItem;
    }

    @Override
    public boolean applicableTo(final LocalePair pair) {
        if (!this.read) {
            fromTableItem();
        }
        return this.defaultFontMapping.applicableTo(pair);
    }

    @Override
    public boolean applicableTo(final String sourceFont) {
        if (!this.read) {
            fromTableItem();
        }
        return this.defaultFontMapping.applicableTo(sourceFont);
    }

    @Override
    public String targetFont() {
        if (!this.read) {
            fromTableItem();
        }
        return this.defaultFontMapping.targetFont();
    }

    private void fromTableItem() {
        this.defaultFontMapping = new DefaultFontMapping(
            this.tableItem.getText(0),
            this.tableItem.getText(1),
            this.tableItem.getText(2),
            this.tableItem.getText(3)
        );
        this.read = true;
    }

    @Override
    public <T> T writtenTo(final Output<T> output) {
        if (!this.read) {
            fromTableItem();
        }
        return this.defaultFontMapping.writtenTo(output);
    }
}
